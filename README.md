# Akka Java
 
provides common bootstrap and helper classes

## Akka System

use guice to initialize akka world, enable DI and helper classes for configuring logic in actors

## Akka Message

common message used for communication

## Akka Test Helper

common test patterns for actors. provides utility classes to assert communication flow

## Async Client

common classes to communicate over TCP