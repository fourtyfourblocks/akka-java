/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.akka.message;

/**
 * @author pawelkaminski
 *description : generic ack message
 */
public class Ok extends Message
{
    private static final long serialVersionUID = 1027259717430475400L;

    public Ok()
    {
    }

    public Ok(String mdc)
    {
        super(mdc);
    }

    @Override
    protected StringBuilder appendToString(StringBuilder sb)
    {
        return sb.append("ok");
    }
}
    
