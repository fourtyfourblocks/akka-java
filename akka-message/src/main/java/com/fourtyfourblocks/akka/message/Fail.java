/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.akka.message;

/**
 * @author pawelkaminski
 *
 */
public class Fail<T> extends Message
{
    private static final long serialVersionUID = 1061353500140554300L;
    private final T error;

    public Fail(T error)
    {
        this.error = error;
    }

    public Fail(String mdc, T error)
    {
        super(mdc);
        this.error = error;
    }

    public T getError()
    {
        return error;
    }

    @Override
    protected StringBuilder appendToString(StringBuilder sb)
    {
        return sb.append("error=").append(error);
    }
}
    
