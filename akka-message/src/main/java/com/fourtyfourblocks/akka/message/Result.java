/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.akka.message;

/**
 * @author pawelkaminski
 *description : generic message with computed result
 */
public class Result<T> extends Message
{
    private static final long serialVersionUID = 6489075690948291528L;

    private final T value;

    public Result(T value)
    {
        this.value = value;
    }

    public T getValue()
    {
        return value;
    }

    @Override
    protected StringBuilder appendToString(StringBuilder sb)
    {
        return sb.append("value=").append(value);
    }
}
    
