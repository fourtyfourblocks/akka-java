/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.akka.message;

import org.slf4j.MDC;

import java.io.Serializable;
import java.util.UUID;

/**
 * @author pawelkaminski
 *
 */
public abstract class Message implements Serializable
{
    private static final long serialVersionUID = -1151669624897300051L;

    private final String mdc;
    public final static String MDC_LOGGER_KEY = "id";

    public Message()
    {
        String id = MDC.get(MDC_LOGGER_KEY);
        this.mdc = id == null ? UUID.randomUUID().toString() : id;
    }

    public Message(String mdc)
    {
        this.mdc = mdc;
    }

    public Message(Message msg)
    {
        this.mdc = msg.getMDC();
    }

    public String getMDC()
    {
        return mdc;
    }

    protected abstract StringBuilder appendToString(StringBuilder sb);

    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder(this.getClass().getSimpleName());
        sb.append("{MDC='").append(mdc).append("',");
        appendToString(sb);
        sb.append('}');
        return sb.toString();
    }
}
    
