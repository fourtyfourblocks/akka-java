/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.akka;

import akka.actor.*;
import akka.japi.Creator;

import java.util.concurrent.TimeUnit;

/**
 * @author pawelkaminski
 *
 */
public interface Actor
{
    <T> boolean is(Object message, Class<T> clazz);

    <T> T cast(Object message, Class<T> clazz);

    void tell(ActorRef pid, Object message);

    void tell(String path, Object message);

    void identify(String path, String id);
    String identify(String path);

    void kill(ActorRef pid);

    void poison(ActorRef pid);

    void stop(ActorRef pid);

    <T> void replay(T message);

    Props configure(Creator<? extends UntypedActor> creator);

    Props configure(Class<? extends UntypedActor> clazz);

    ActorRef create(Props props);

    ActorRef createAndWatch(Props props);

    ActorRef create(Props props, String uniqueName);

    ActorRef createAndWatch(Props props, String uniqueName);

    void watch(ActorRef actorRef);

    void requestTimeout(int duration, TimeUnit unit);

    void stopTimeout();

    void become(Behaviour behaviour);

    void pushBehaviour(Behaviour behaviour);

    void popBehaviour();
}
