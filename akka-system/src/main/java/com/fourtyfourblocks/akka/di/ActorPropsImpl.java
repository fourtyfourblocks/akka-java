/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.akka.di;

import akka.actor.UntypedActor;

import java.lang.annotation.Annotation;

/**
 * @author pawel
 *
 */
public class ActorPropsImpl implements ActorProps
{
    private final Class<? extends UntypedActor> aClass;

    public ActorPropsImpl(Class<? extends UntypedActor> aClass)
    {
        this.aClass = aClass;
    }

    @Override
    public Class<? extends UntypedActor> actorClass()
    {
        return aClass;
    }

    @Override
    public Class<? extends Annotation> annotationType()
    {
        return ActorProps.class;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        ActorPropsImpl that = (ActorPropsImpl) o;

        if (!aClass.equals(that.aClass))
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        return (127 * "actorClass".hashCode()) ^ aClass.hashCode();
    }
}
    
