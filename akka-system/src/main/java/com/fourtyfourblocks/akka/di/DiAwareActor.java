/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.akka.di;

import akka.actor.Props;
import akka.actor.UntypedActor;
import com.fourtyfourblocks.akka.GenericActor;

import javax.inject.Inject;

/**
 * @author pawelkaminski
 *
 */
public abstract class DiAwareActor extends GenericActor
{
    @Inject
    private DiAwareCreatorFactory factory;

    @Override
    public Props configure(Class<? extends UntypedActor> clazz)
    {
        return super.configure(factory.creator(clazz));
    }
}
    
