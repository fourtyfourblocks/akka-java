/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.akka.di.guice;

import akka.actor.UntypedActor;
import akka.japi.Creator;
import com.google.inject.Injector;

/**
 * @author pawelkaminski
 *
 */
class GuiceAwareCreator<T extends UntypedActor> implements Creator<T>
{
    private final Class<T> actorType;
    private final Injector injector;

    public GuiceAwareCreator(Class<T> actorType, Injector injector)
    {
        this.actorType = actorType;
        this.injector = injector;
    }

    @Override
    public T create() throws Exception
    {
        return injector.getInstance(actorType);
    }
}

    
