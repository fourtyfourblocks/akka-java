/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.akka.di;

import com.fourtyfourblocks.akka.TypeHelper;
import com.google.inject.AbstractModule;

/**
 * @author pawelkaminski
 *
 */
public class BaseModule extends AbstractModule
{
    @Override
    public void configure()
    {
        bind(TypeHelper.class);
    }
}
    
