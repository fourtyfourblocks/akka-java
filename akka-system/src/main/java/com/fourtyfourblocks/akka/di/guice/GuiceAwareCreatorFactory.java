/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.akka.di.guice;

import akka.actor.UntypedActor;
import akka.japi.Creator;
import com.fourtyfourblocks.akka.di.DiAwareCreatorFactory;
import com.google.inject.Injector;

/**
 * @author pawelkaminski
 *
 */
public class GuiceAwareCreatorFactory implements DiAwareCreatorFactory
{
    private final Injector injector;

    public GuiceAwareCreatorFactory(Injector injector)
    {
        this.injector = injector;
    }

    public <T extends UntypedActor> Creator<T> creator(Class<T> actor)
    {
        return new GuiceAwareCreator<>(actor, injector);
    }
}
    
