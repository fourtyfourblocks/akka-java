/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.akka.di;

import akka.actor.UntypedActor;
import akka.japi.Creator;

/**
 * @author pawelkaminski
 *
 */
public interface DiAwareCreatorFactory
{
    <T extends UntypedActor> Creator<T> creator(Class<T> actor);
}
