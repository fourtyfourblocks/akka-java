/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.akka.di;

import akka.actor.UntypedActor;

import javax.inject.Qualifier;
import java.lang.annotation.*;

/**
 * @author pawelkaminski
 *
 */
@Qualifier
@Documented
@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ActorProps
{
    Class<? extends UntypedActor> actorClass();
}
