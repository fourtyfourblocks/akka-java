package com.fourtyfourblocks.akka.di.guice;

import com.fourtyfourblocks.akka.di.DiAwareCreatorFactory;
import com.google.inject.*;

public class GuiceDiModule extends AbstractModule{

    @Provides
    @Singleton
    public DiAwareCreatorFactory getFactory(Injector injector)
    {
        return new GuiceAwareCreatorFactory(injector);
    }

    @Override
    protected void configure() {

    }
}
