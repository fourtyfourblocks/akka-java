package com.fourtyfourblocks.akka;

public class TypeHelper {
    public <T> boolean is(Object message, Class<T> clazz) {
        return message != null && (message.getClass() == clazz || clazz.isInstance(message));
    }

    public <T> T cast(Object message, Class<T> clazz) {
        return clazz.cast(message);
    }
}