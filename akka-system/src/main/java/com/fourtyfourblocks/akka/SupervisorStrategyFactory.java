/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.akka;

import akka.actor.*;
import akka.japi.Function;
import scala.concurrent.duration.Duration;

import java.util.concurrent.TimeUnit;

import static akka.actor.SupervisorStrategy.Directive;


/**
 * @author pawelkaminski
 *
 */

public class SupervisorStrategyFactory
{
    public final static Function<Throwable, Directive> stopDecider = new Function<Throwable, Directive>()
    {
        @Override
        public Directive apply(Throwable throwable) throws Exception
        {
            return SupervisorStrategy.stop();
        }
    };
    public final static Function<Throwable, Directive> resumeDecider = new Function<Throwable, Directive>()
    {
        @Override
        public Directive apply(Throwable throwable) throws Exception
        {
            return SupervisorStrategy.resume();
        }
    };
    public final static Function<Throwable, Directive> resumeOrStopDecider = new Function<Throwable, Directive>()
    {
        @Override
        public Directive apply(Throwable throwable) throws Exception
        {
            Directive strategy = SupervisorStrategy.resume();
            if (throwable instanceof IllegalStateException ||
                throwable instanceof IllegalArgumentException)
            {
                strategy = SupervisorStrategy.stop();
            }
            return strategy;
        }
    };
    public final static Function<Throwable, Directive> escalateDecider = new Function<Throwable, Directive>()
    {
        @Override
        public Directive apply(Throwable throwable) throws Exception
        {
            return SupervisorStrategy.escalate();
        }
    };
    public final static Function<Throwable, Directive> restartDecider = new Function<Throwable, Directive>()
    {
        @Override
        public Directive apply(Throwable throwable) throws Exception
        {
            return SupervisorStrategy.restart();
        }
    };
    public final static Function<Throwable, Directive> suicideDecider = new Function<Throwable, Directive>()
    {
        @Override
        public Directive apply(Throwable throwable) throws Exception
        {
            return SupervisorStrategy.escalate();
        }
    };

    public static SupervisorStrategy oneForOne(int maxRetries, int duringSec)
    {
        return new OneForOneStrategy(maxRetries, Duration.create(duringSec, TimeUnit.SECONDS), stopDecider);
    }

    public static SupervisorStrategy allForOne(int maxRetries, int duringSec)
    {
        return new AllForOneStrategy(maxRetries, Duration.create(duringSec, TimeUnit.SECONDS), stopDecider);
    }

    public static SupervisorStrategy oneForOne(int maxRetries, int duringSec, Function<Throwable, Directive> decider)
    {
        return new OneForOneStrategy(maxRetries, Duration.create(duringSec, TimeUnit.SECONDS), decider);
    }

    public static SupervisorStrategy allForOne(int maxRetries, int duringSec, Function<Throwable, Directive> decider)
    {
        return new AllForOneStrategy(maxRetries, Duration.create(duringSec, TimeUnit.SECONDS), decider);
    }
}
    
