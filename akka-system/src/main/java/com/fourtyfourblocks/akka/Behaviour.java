/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.akka;

import akka.japi.Procedure;
import com.fourtyfourblocks.akka.message.Message;
import org.slf4j.MDC;

/**
 * @author pawelkaminski
 *
 */
public abstract class Behaviour implements Procedure<Object>
{
    protected final Actor actor;

    public Behaviour(Actor actor)
    {
        this.actor = actor;
    }

    @Override
    public void apply(Object o) throws Exception
    {
        if (is(o, Message.class))
        {
            MDC.put(Message.MDC_LOGGER_KEY, ((Message) o).getMDC());
        }
        onMessage(o);

        MDC.remove(Message.MDC_LOGGER_KEY);
    }

    public <T> boolean is(Object message, Class<T> clazz)
    {
        return actor.is(message, clazz);
    }

    public <T> T cast(Object message, Class<T> clazz)
    {
        return actor.cast(message, clazz);
    }

    public abstract void onMessage(Object message) throws Exception;
}