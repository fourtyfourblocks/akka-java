/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.asyncclient.netty.http.json.ning;

import com.fourtyfourblocks.asyncclient.AsyncResponse;
import com.fourtyfourblocks.asyncclient.http.json.*;
import com.ning.http.client.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.concurrent.Future;

/**
 * @author pawelkaminski
 *
 */
public class AsyncRestClient {
    private final static Logger logger = LoggerFactory.getLogger(AsyncRestClient.class);

    private final com.ning.http.client.AsyncHttpClient asyncHttpClient;
    private final JsonMarshallerStrategy strategy;

    public AsyncRestClient(AsyncHttpClient asyncHttpClient, JsonMarshallerStrategy strategy) {
        this.asyncHttpClient = asyncHttpClient;
        this.strategy = strategy;
    }

    @SuppressWarnings("unchecked")
    public <T> Future<T> get(final String url, final Class<T> type, final AsyncResponse<T> handler) throws IOException {
        final JsonMarshaller<T> marshaller = strategy.getSimpleTypeMarshaller(type);
        return asyncHttpClient.
                prepareGet(url).
                addHeader("Content-Type", "application/json;charset=UTF-8").
                addHeader("X-Mdc", handler.getMdc()).
                execute(new Handler(handler, marshaller));
    }

    @SuppressWarnings("unchecked")
    public <T> Future<ListResult<T>> getListResult(final String url, final Class<T> type,
                                                   final AsyncResponse<ListResult<T>> handler) throws IOException {
        final JsonMarshaller<ListResult<T>> marshaller = strategy.getListResultMarshaller(type);
        return asyncHttpClient.
                prepareGet(url).
                addHeader("Content-Type", "application/json;charset=UTF-8").
                addHeader("X-Mdc", handler.getMdc()).
                execute(new Handler(handler, marshaller));
    }

    @SuppressWarnings("unchecked")
    public <T> Future<T> post(final String url, final Class<T> type, Object body, final AsyncResponse<T> handler) throws
            IOException {
        final JsonMarshaller<T> marshaller = strategy.getSimpleTypeMarshaller(type);
        return asyncHttpClient.
                preparePost(url).
                setBody(marshaller.marshall(body)).
                addHeader("Content-Type", "application/json;charset=UTF-8").
                addHeader("X-Mdc", handler.getMdc()).
                execute(new Handler(handler, marshaller));
    }

    @SuppressWarnings("unchecked")
    public <T> Future<ListResult<T>> postListResult(String url, Class<T> type, Object body, AsyncResponse<ListResult<T>> handler) throws
            IOException {
        final JsonMarshaller<ListResult<T>> marshaller = strategy.getListResultMarshaller(type);
        return asyncHttpClient.
                preparePost(url).
                setBody(marshaller.marshall(body)).
                addHeader("Content-Type", "application/json;charset=UTF-8").
                addHeader("X-Mdc", handler.getMdc()).
                execute(new Handler(handler, marshaller));
    }

    private String encode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    private class Handler<T> extends AsyncCompletionHandler<T> {
        private final AsyncResponse<T> handler;
        private final JsonMarshaller<T> marshaller;

        private Handler(AsyncResponse<T> handler, JsonMarshaller<T> marshaller) {
            this.handler = handler;
            this.marshaller = marshaller;
        }

        @Override
        public T onCompleted(Response response) throws Exception {
            T data = null;
            final int statusCode = response.getStatusCode();
            if (statusCode != 500) {
                data = marshaller.unmarshall(response.getResponseBodyAsStream());
                handler.success(data, statusCode);
            } else {
                handler.error(encode(response.getResponseBody()));
            }

            return data;
        }

        @Override
        public void onThrowable(Throwable t) {
            logger.error("Error while sending request", t);
            handler.error("Error while sending request");
        }
    }
}
    
