package com.fourtyfourblocks.asyncclient.netty.http.json.ning;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fourtyfourblocks.asyncclient.http.json.JsonMarshallerStrategy;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.ning.http.client.AsyncHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Singleton;

public class AsyncClientJsonModule extends AbstractModule
{
    private final static Logger logger = LoggerFactory.getLogger(AsyncClientJsonModule.class);

    private ObjectMapper mapper = new ObjectMapper();
    private AsyncHttpClient asyncClient = new AsyncHttpClient();
    private JsonMarshallerStrategy strategy;

    @PostConstruct
    public void init()
    {
        strategy = new JsonMarshallerStrategy(mapper);
    }

    @Provides
    @Singleton
    public AsyncRestClient newClient(JsonMarshallerStrategy strategy)
    {
        logger.debug("creating new async client.");
        return new AsyncRestClient(asyncClient, strategy);
    }

    @Override
    protected void configure() {

    }
}
