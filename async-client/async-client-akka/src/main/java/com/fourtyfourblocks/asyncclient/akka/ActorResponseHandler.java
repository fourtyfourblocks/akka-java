/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.asyncclient.akka;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import com.fourtyfourblocks.akka.message.Fail;
import com.fourtyfourblocks.akka.message.Message;
import com.fourtyfourblocks.asyncclient.AsyncResponse;
import org.slf4j.MDC;

import java.util.UUID;

/**
 * @author pawelkaminski
 *
 */
public abstract class ActorResponseHandler<T> implements AsyncResponse<T>
{
    private final ActorRef invoker;
    private final ActorRef sender;
    private final String mdc;
    // todo add logger from actor

    protected ActorResponseHandler(UntypedActor caller)
    {
        this(caller.getSelf(), caller.getSender());
    }

    protected ActorResponseHandler(ActorRef invoker, ActorRef sender)
    {
        this.invoker = invoker;
        this.sender = sender;
        final String mdc = MDC.get(Message.MDC_LOGGER_KEY);
        this.mdc = (mdc == null ? UUID.randomUUID().toString() : mdc);
    }

    @Override
    public void success(T data, int statusCode)
    {
        MDC.put(Message.MDC_LOGGER_KEY, mdc);
        onSuccess(invoker, sender, data);
    }

    @Override
    public void error(Object error)
    {
        onError(sender, new Fail<>(mdc, error));
    }

    public void onSuccess(ActorRef invoker, ActorRef sender, T data)
    {
        sender.tell(data, invoker);
    }

    public void onError(ActorRef sender, Object error)
    {
        sender.tell(error, null);
    }

    @Override
    public String getMdc()
    {
        return mdc;
    }
}