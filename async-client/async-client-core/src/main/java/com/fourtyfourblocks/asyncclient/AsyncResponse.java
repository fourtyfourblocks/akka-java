/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.asyncclient;

/**
 * @author pawelkaminski
 *
 */
public interface AsyncResponse<T>
{
    void success(T data, int statusCode);

    void error(Object error);

    String getMdc();
}
    
