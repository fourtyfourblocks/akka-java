/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.asyncclient.http.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author pawelkaminski
 *
 */
public class SimpleTypeMarshaller<T> implements JsonMarshaller<T>
{
    private final static Logger logger = LoggerFactory.getLogger(SimpleTypeMarshaller.class);
    private final ObjectMapper mapper;
    private final Class<T> type;

    public SimpleTypeMarshaller(ObjectMapper mapper, Class<T> type)
    {
        this.mapper = mapper;
        this.type = type;
    }

    @Override
    public byte[] marshall(Object body) throws IOException
    {
        logger.debug("Marshalling body object {}.", body);
        return mapper.writeValueAsBytes(body);
    }

    @Override
    public T unmarshall(InputStream body) throws IOException
    {
        final T data = mapper.readValue(body, type);
        logger.debug("Unmarshalled body to {}.", data);

        return data;
    }
}
    
