/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.asyncclient.http.json;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author pawelkaminski
 *
 */
public interface JsonMarshaller<T>
{
    byte[] marshall(Object body) throws IOException;

    T unmarshall(InputStream body) throws IOException;
}
    
