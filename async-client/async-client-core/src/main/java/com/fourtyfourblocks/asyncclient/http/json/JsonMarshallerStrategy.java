/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.asyncclient.http.json;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author pawelkaminski
 *
 */
public class JsonMarshallerStrategy
{
    private final ObjectMapper mapper;

    public JsonMarshallerStrategy(ObjectMapper mapper)
    {
        this.mapper = mapper;
    }

    public <T> JsonMarshaller<T> getSimpleTypeMarshaller(Class<T> type)
    {
        return new SimpleTypeMarshaller<>(mapper, type);
    }

    public <T> JsonMarshaller<ListResult<T>> getListResultMarshaller(Class<T> type)
    {
        return new ListResultMarshaller<>(mapper, type);
    }
}
    
