/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.asyncclient.http.json;

import java.util.Collection;
import java.util.LinkedList;

/**
 * @author pawelkaminski
 *
 */
public class ListResult<T>
{

    private long offset;
    private long limit;
    private long totalFound;
    private Collection<T> content;

    public ListResult(Collection<T> items)
    {
        this.content = items;
        this.totalFound = items.size();
    }

    public long getOffset()
    {
        return offset;
    }

    public void setOffset(long offset)
    {
        this.offset = offset;
    }

    public long getLimit()
    {
        return limit;
    }

    public void setLimit(long limit)
    {
        this.limit = limit;
    }

    public long getTotalFound()
    {
        return totalFound;
    }

    public void setTotalFound(long totalFound)
    {
        this.totalFound = totalFound;
    }

    public Collection<T> getContent()
    {
        return content;
    }

    public void setContent(Collection<T> content)
    {
        this.content = content;
    }

    public void addContentElement(T element)
    {
        if (this.content == null)
        {
            this.content = new LinkedList<T>();
        }
        this.content.add(element);
    }

    public static <E> ListResult with(Collection<E> entity)
    {
        ListResult<E> result = new ListResult<E>(entity);
        return result;
    }

    @Override
    public String toString()
    {
        return "ListResult{" +
               "offset=" + offset +
               ", limit=" + limit +
               ", totalFound=" + totalFound +
               ", content=" + content +
               '}';
    }
}
    
