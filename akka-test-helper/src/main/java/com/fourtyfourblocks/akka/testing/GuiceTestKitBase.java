/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.akka.testing;

import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.JavaTestKit;
import akka.testkit.TestActorRef;
import com.fourtyfourblocks.akka.GenericActor;
import com.fourtyfourblocks.akka.di.DiAwareCreatorFactory;
import com.fourtyfourblocks.akka.di.guice.GuiceAwareCreatorFactory;
import com.google.inject.*;
import org.junit.After;
import org.junit.Before;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.StrictAssertions.assertThat;

/**
 * @author pawelkaminski
 *
 */
public abstract class GuiceTestKitBase<T extends GenericActor>
{
    protected ActorSystem system;
    protected JavaTestKit messageProbe;
    protected JavaTestKit watchingProbe;
    protected TestActorRef<T> testActorRef;

    private Injector injector;

    private Module defaultEnv = new Module()
    {
        @Override
        public void configure(Binder binder)
        {
        }

        @Provides
        public DiAwareCreatorFactory getFactory(Injector injector)
        {
            return new GuiceAwareCreatorFactory(injector);
        }
    };

    @Before
    public void setUp() throws Exception
    {
        system = ActorSystem.create("test-system");
        messageProbe = new JavaTestKit(system);
        watchingProbe = new JavaTestKit(system);

        before();
    }

    protected abstract void before() throws Exception;

    @After
    public void tearDown() throws Exception
    {
        after(watchingProbe);

        watchingProbe.unwatch(testActorRef);
        system.shutdown();
    }

    protected void after(JavaTestKit watchingProbe) throws Exception
    {
    }

    protected Injector getInjector(Module env)
    {
        if (env == null)
        {
            throw new IllegalArgumentException("Env cannot be null.");
        }
        injector = Guice.createInjector(Stage.DEVELOPMENT, defaultEnv, env);
        return injector;
    }

    protected TestActorRef<T> createDefaultTestActor(Class<T> clazz, String actorName)
    {
        testActorRef = createTestActor(clazz, actorName);
        return testActorRef;
    }

    protected <C extends GenericActor> TestActorRef<C> createTestActor(Class<C> clazz, String actorName)
    {
        final DiAwareCreatorFactory factory = injector.getInstance(DiAwareCreatorFactory.class);
        final Props props = Props.create(factory.creator(clazz));
        TestActorRef<C> testActorRef = TestActorRef.create(system, props, actorName);
        watchingProbe.watch(testActorRef);

        return testActorRef;
    }

    protected <T> List<T> expectCountMsgOfType(int count, Class<T> type)
    {
        return expectCountMsgOfType(messageProbe, count, type);
    }

    protected <T> List<T> expectCountMsgOfType(JavaTestKit probe, int count, Class<T> type)
    {
        final Object[] messages = probe.receiveN(count);
        assertThat(messages).hasSize(count);

        List<T> msgs = new ArrayList<>(count);
        for (Object message : messages)
        {
            assertThat(message).isInstanceOf(type);
            msgs.add(type.cast(message));
        }

        probe.expectNoMsg();

        return msgs;
    }

    protected <T> List<T> findAll(Object[] msgs, Class<T> aClass)
    {
        List<T> all = new ArrayList<>();
        for (Object msg : msgs)
        {
            if (aClass.isInstance(msg))
            {
                all.add(aClass.cast(msg));
            }
        }

        return all;
    }
}
    
