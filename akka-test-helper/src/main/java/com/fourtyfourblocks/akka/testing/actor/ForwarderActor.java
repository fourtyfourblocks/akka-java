/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.akka.testing.actor;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;

/**
 * this is a copy of https://gist.github.com/jconwell/8153746#file-forwarderactor-java
 * <br>
 * Simple actor that takes another actor and forwards all messages to it.
 * Useful in unit testing for capturing and testing if a message was received.
 * Simply pass in an Akka JavaTestKit probe into the constructor, and all messages
 * that are sent to this actor are forwarded to the JavaTestKit probe
 *
 * @author pawel
 */
public class ForwarderActor extends UntypedActor
{
    final ActorRef target;

    public ForwarderActor(ActorRef target)
    {
        this.target = target;
    }

    @Override
    public void onReceive(Object msg)
    {
        target.forward(msg, getContext());
    }
}