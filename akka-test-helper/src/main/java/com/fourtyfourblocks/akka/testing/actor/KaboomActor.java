/*
 * Software is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.
 * 
 * The Initial Developer of the Original Code is Paweł Kamiński.
 * All Rights Reserved.
 */
package com.fourtyfourblocks.akka.testing.actor;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;

/**
 * @author pawelkaminski
 *
 */
public class KaboomActor extends UntypedActor
{
    final ActorRef target;

    public KaboomActor(ActorRef target)
    {
        this.target = target;
    }

    @Override
    public void preStart() throws Exception
    {
        target.tell(self(), self());
    }

    @Override
    public void onReceive(Object o) throws Exception
    {
        if (o instanceof Exception)
        {
            throw (Exception) o;
        }

        target.forward(o, getContext());
    }
}
    
